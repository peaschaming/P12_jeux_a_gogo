package com.peasch.jeuxagogo.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeuxagogoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JeuxagogoApplication.class, args);
    }

}
